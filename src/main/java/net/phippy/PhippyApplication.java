package net.phippy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhippyApplication {
    public static void main(String[] args) {
        SpringApplication.run(PhippyApplication.class, args);
    }
}
