package net.phippy.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    public SecurityScheme secureAPI() {
        return new SecurityScheme()
                .name("Secure")
                .type(SecurityScheme.Type.HTTP)
                .scheme("Bearer")
                .bearerFormat("JWT");
    }

    @Bean
    public OpenAPI myOpenAPI() {
        return new OpenAPI().components(new Components()
                .addSecuritySchemes("Secure", secureAPI())).info(
                new Info().contact(new Contact()
                                .name("Pavel Špička")
                                .email("spicpav@gmail.com")
                                .url("https://phippy.net/"))
                        .description("Phippy - Sensors Manager")
        );
    }
}
