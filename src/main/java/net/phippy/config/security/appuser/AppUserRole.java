package net.phippy.config.security.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
