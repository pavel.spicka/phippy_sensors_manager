package net.phippy.config.security.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.phippy.config.security.appuser.AppUser;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VerifyResponse {
    private AppUser userDetails;
    private SuccessFailure status;
    private String message;

    public enum SuccessFailure {
        SUCCESS, FAILURE
    }
}
