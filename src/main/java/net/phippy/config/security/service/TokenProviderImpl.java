package net.phippy.config.security.service;

import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class TokenProviderImpl implements TokenProvider {
    @Value("${authentication.auth.tokenSecret}")
    private String tokenSecret;

    @Override
    public String getEmailFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }

    @Override
    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(tokenSecret).parse(token);
            return true;
        } catch (SignatureException ex) {
            log.info("validateToken : " + ex.getMessage());
        } catch (MalformedJwtException ex) {
            log.info("validateToken : " + ex.getMessage());
        } catch (ExpiredJwtException ex) {
            log.info("validateToken : " + ex.getMessage());
        } catch (UnsupportedJwtException ex) {
            log.info("validateToken : " + ex.getMessage());
        } catch (IllegalArgumentException ex) {
            log.info("validateToken : " + ex.getMessage());
        }
        return false;
    }
}
