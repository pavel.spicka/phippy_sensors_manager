package net.phippy.config.security.service;

public interface TokenProvider {
    String getEmailFromToken(String token);
    boolean validateToken(String token);
}
