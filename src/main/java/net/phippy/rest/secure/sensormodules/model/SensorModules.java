package net.phippy.rest.secure.sensormodules.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.phippy.rest.secure.sensormodules.model.modules.Telemetry;

import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorModules {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) private Long id;
    @Column(unique=true) private String location;
    private String apiKey;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Telemetry> telemetries = new HashSet<>();

    public SensorModules(String location, String apiKey) { this.location = location; this.apiKey = apiKey; }

    public final Set<Telemetry> getTelemetry() {
        return telemetries;
    }

    public void addTelemetry(Telemetry telemetry){ telemetries.add(telemetry); }

    public void removeTelemetry(Telemetry telemetry){ telemetries.remove(telemetry); }
}
