package net.phippy.rest.secure.sensormodules.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.phippy.rest.secure.sensormodules.model.modules.Telemetry;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorModulesLastValueResponse {
    private Long id;
    private String location;
    private String apiKey;
    private Telemetry telemetry;
}
