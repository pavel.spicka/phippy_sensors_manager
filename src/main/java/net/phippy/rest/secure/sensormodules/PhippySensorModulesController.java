package net.phippy.rest.secure.sensormodules;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.phippy.rest.secure.sensormodules.model.SensorModules;
import net.phippy.rest.secure.sensormodules.model.modules.Telemetry;
import net.phippy.rest.secure.sensormodules.model.SensorModulesLastValueResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping(path = "/api/v1/sensormodules")
public class PhippySensorModulesController {
    private final SensorModulesRepository sensorModulesRepository;

    @GetMapping({"/telemetry"})
    public ResponseEntity<SensorModules> telemetry(@Valid @RequestParam(defaultValue = "", value = "location") String location) {
        Optional<SensorModules> sensorModulesOptional = sensorModulesRepository.findSensorModulesByLocation(location);
        return sensorModulesOptional.map(sensorModules -> ResponseEntity.ok().body(sensorModules))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping({"/lastTelemetry"})
    public ResponseEntity<Telemetry> lastTelemetry(@Valid @RequestParam(defaultValue = "", value = "location") String location) {
        Optional<SensorModules> sensorModulesOptional = sensorModulesRepository.findSensorModulesByLocation(location);
        return sensorModulesOptional.map(sensorModules ->
                        ResponseEntity.ok().body(sensorModules.getTelemetries().stream().reduce((prev, next) -> next).orElse(null)))
                .orElseGet(() -> ResponseEntity.badRequest().build());
    }

    @GetMapping({"/lastTelemetryAllLocations"})
    public ResponseEntity<List<SensorModulesLastValueResponse>> lastTelemetryAllLocations() {
        return ResponseEntity.ok().body(sensorModulesRepository.findAll().stream().map(sensorModules ->
                        new SensorModulesLastValueResponse(sensorModules.getId(),
                                sensorModules.getLocation(),
                                sensorModules.getApiKey(),
                                sensorModules.getTelemetries().stream().reduce((prev, next) -> next).orElse(null)))
                .collect(Collectors.toCollection(ArrayList::new)));
    }
}
