package net.phippy.rest.secure.sensormodules.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SensorModulesLocationResponse {
    private Long id;
    private String location;
    private String apiKey;
}
