package net.phippy.rest.secure.sensormodules;

import io.swagger.v3.oas.annotations.Hidden;
import net.phippy.rest.secure.sensormodules.model.SensorModules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Hidden
@Repository
public interface SensorModulesRepository extends JpaRepository<SensorModules, Long> {
    Optional<SensorModules> findSensorModulesByLocation(String location);
}
