package net.phippy.rest.secure.sensormodules.model.modules;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Telemetry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) private Long id;
    private String location;
    @Column(nullable = false)
    private LocalDateTime createdAt;
    private double temperature;
    private double pressure;
    private double altitude;

    public Telemetry(String location, LocalDateTime createdAt, double temperature, double pressure, double altitude) {
        this.location = location;
        this.createdAt = createdAt;
        this.temperature = temperature;
        this.pressure = pressure;
        this.altitude = altitude;
    }
}
