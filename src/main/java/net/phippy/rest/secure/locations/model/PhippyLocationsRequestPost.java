package net.phippy.rest.secure.locations.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhippyLocationsRequestPost {
    private String location;
    private String apiKey;
}
