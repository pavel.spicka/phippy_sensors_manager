package net.phippy.rest.secure.locations;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.phippy.rest.secure.sensormodules.SensorModulesRepository;
import net.phippy.rest.secure.sensormodules.model.SensorModules;
import net.phippy.rest.secure.locations.model.PhippyLocationsRequestPost;
import net.phippy.rest.secure.sensormodules.model.SensorModulesLocationResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping(path = "/api/v1/sensormodules")
public class PhippyLocationsController {
    private final SensorModulesRepository sensorModulesRepository;

    @PostMapping({"/locations"})
    public ResponseEntity<?> locations(@Valid @RequestBody PhippyLocationsRequestPost locationsRequestPost) {
        sensorModulesRepository.save(new SensorModules(locationsRequestPost.getLocation(), locationsRequestPost.getApiKey()));
        return ResponseEntity.ok().build();
    }

    @GetMapping({"/allLocations"})
    public ResponseEntity<List<SensorModulesLocationResponse>> allLocations() {
        return ResponseEntity.ok().body(sensorModulesRepository.findAll().stream().map(sensorModules ->
                        new SensorModulesLocationResponse(sensorModules.getId(), sensorModules.getLocation(), sensorModules.getApiKey()))
                .collect(Collectors.toCollection(ArrayList::new)));
    }
}

