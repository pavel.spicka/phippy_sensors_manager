package net.phippy.rest.api;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.phippy.rest.secure.sensormodules.SensorModulesRepository;
import net.phippy.rest.secure.sensormodules.model.SensorModules;
import net.phippy.rest.secure.sensormodules.model.modules.Telemetry;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Optional;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping(path = "/open-api/weather")
public class OpenApiWeatherController {
    private final SensorModulesRepository sensorModulesRepository;

    @PostMapping(value = {"/post-data/{apiKey}"})
    public ResponseEntity<?> postData(@PathVariable(value = "apiKey") final String apiKey,
                                   @Valid @RequestParam(defaultValue = "", value = "location") String location,
                                   @Valid @RequestParam(defaultValue = "0", value = "temperature") double temperature,
                                   @Valid @RequestParam(defaultValue = "0", value = "pressure") double pressure,
                                   @Valid @RequestParam(defaultValue = "0", value = "altitude") double altitude) {
        Optional<SensorModules> sensorModulesOptional = sensorModulesRepository.findSensorModulesByLocation(location);
        if (sensorModulesOptional.isPresent() && sensorModulesOptional.get().getApiKey().equals(apiKey)) {
            sensorModulesOptional.get().addTelemetry(new Telemetry(location,
                    LocalDateTime.now(),
                    temperature,
                    pressure,
                    altitude));
            sensorModulesRepository.save(sensorModulesOptional.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

}
